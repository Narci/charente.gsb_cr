﻿namespace GSB_CR
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_CR_NumRapp = new System.Windows.Forms.TextBox();
            this.tb_CR_DateRapp = new System.Windows.Forms.TextBox();
            this.tb_CR_Visite = new System.Windows.Forms.TextBox();
            this.cb_CR_Praticien = new System.Windows.Forms.ComboBox();
            this.rtb_CR_Bilan = new System.Windows.Forms.RichTextBox();
            this.btn_CR_Details = new System.Windows.Forms.Button();
            this.btn_CR_Precedent = new System.Windows.Forms.Button();
            this.btn_CR_Suivant = new System.Windows.Forms.Button();
            this.btn_CR_Nouveau = new System.Windows.Forms.Button();
            this.btn_CR_Fermer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tb_CR_NumRapp
            // 
            this.tb_CR_NumRapp.Location = new System.Drawing.Point(115, 12);
            this.tb_CR_NumRapp.Name = "tb_CR_NumRapp";
            this.tb_CR_NumRapp.Size = new System.Drawing.Size(121, 20);
            this.tb_CR_NumRapp.TabIndex = 0;
            // 
            // tb_CR_DateRapp
            // 
            this.tb_CR_DateRapp.Location = new System.Drawing.Point(115, 69);
            this.tb_CR_DateRapp.Name = "tb_CR_DateRapp";
            this.tb_CR_DateRapp.Size = new System.Drawing.Size(121, 20);
            this.tb_CR_DateRapp.TabIndex = 1;
            // 
            // tb_CR_Visite
            // 
            this.tb_CR_Visite.Location = new System.Drawing.Point(115, 95);
            this.tb_CR_Visite.Name = "tb_CR_Visite";
            this.tb_CR_Visite.Size = new System.Drawing.Size(121, 20);
            this.tb_CR_Visite.TabIndex = 3;
            // 
            // cb_CR_Praticien
            // 
            this.cb_CR_Praticien.FormattingEnabled = true;
            this.cb_CR_Praticien.Location = new System.Drawing.Point(115, 42);
            this.cb_CR_Praticien.Name = "cb_CR_Praticien";
            this.cb_CR_Praticien.Size = new System.Drawing.Size(121, 21);
            this.cb_CR_Praticien.TabIndex = 4;
            // 
            // rtb_CR_Bilan
            // 
            this.rtb_CR_Bilan.Location = new System.Drawing.Point(115, 121);
            this.rtb_CR_Bilan.Name = "rtb_CR_Bilan";
            this.rtb_CR_Bilan.Size = new System.Drawing.Size(237, 157);
            this.rtb_CR_Bilan.TabIndex = 5;
            this.rtb_CR_Bilan.Text = "";
            // 
            // btn_CR_Details
            // 
            this.btn_CR_Details.Location = new System.Drawing.Point(242, 42);
            this.btn_CR_Details.Name = "btn_CR_Details";
            this.btn_CR_Details.Size = new System.Drawing.Size(75, 23);
            this.btn_CR_Details.TabIndex = 6;
            this.btn_CR_Details.Text = "Details";
            this.btn_CR_Details.UseVisualStyleBackColor = true;
            this.btn_CR_Details.Click += new System.EventHandler(this.btn_CR_Details_Click);
            // 
            // btn_CR_Precedent
            // 
            this.btn_CR_Precedent.Location = new System.Drawing.Point(115, 284);
            this.btn_CR_Precedent.Name = "btn_CR_Precedent";
            this.btn_CR_Precedent.Size = new System.Drawing.Size(65, 23);
            this.btn_CR_Precedent.TabIndex = 8;
            this.btn_CR_Precedent.Text = "Précédent";
            this.btn_CR_Precedent.UseVisualStyleBackColor = true;
            this.btn_CR_Precedent.Click += new System.EventHandler(this.btn_CR_Precedent_Click);
            // 
            // btn_CR_Suivant
            // 
            this.btn_CR_Suivant.Location = new System.Drawing.Point(186, 284);
            this.btn_CR_Suivant.Name = "btn_CR_Suivant";
            this.btn_CR_Suivant.Size = new System.Drawing.Size(65, 23);
            this.btn_CR_Suivant.TabIndex = 9;
            this.btn_CR_Suivant.Text = "Suivant";
            this.btn_CR_Suivant.UseVisualStyleBackColor = true;
            this.btn_CR_Suivant.Click += new System.EventHandler(this.btn_CR_Suivant_Click);
            // 
            // btn_CR_Nouveau
            // 
            this.btn_CR_Nouveau.Location = new System.Drawing.Point(282, 284);
            this.btn_CR_Nouveau.Name = "btn_CR_Nouveau";
            this.btn_CR_Nouveau.Size = new System.Drawing.Size(65, 23);
            this.btn_CR_Nouveau.TabIndex = 10;
            this.btn_CR_Nouveau.Text = "Nouveau";
            this.btn_CR_Nouveau.UseVisualStyleBackColor = true;
            this.btn_CR_Nouveau.Click += new System.EventHandler(this.btn_CR_Nouveau_Click);
            // 
            // btn_CR_Fermer
            // 
            this.btn_CR_Fermer.Location = new System.Drawing.Point(282, 313);
            this.btn_CR_Fermer.Name = "btn_CR_Fermer";
            this.btn_CR_Fermer.Size = new System.Drawing.Size(65, 23);
            this.btn_CR_Fermer.TabIndex = 11;
            this.btn_CR_Fermer.Text = "Fermer";
            this.btn_CR_Fermer.UseVisualStyleBackColor = true;
            this.btn_CR_Fermer.Click += new System.EventHandler(this.btn_CR_Fermer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Numéro Rapport : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Praticien : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Date Rapport : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Motif Visite : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "BILAN";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(147, 313);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(73, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Sauvgarder";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 356);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_CR_Fermer);
            this.Controls.Add(this.btn_CR_Nouveau);
            this.Controls.Add(this.btn_CR_Suivant);
            this.Controls.Add(this.btn_CR_Precedent);
            this.Controls.Add(this.btn_CR_Details);
            this.Controls.Add(this.rtb_CR_Bilan);
            this.Controls.Add(this.cb_CR_Praticien);
            this.Controls.Add(this.tb_CR_Visite);
            this.Controls.Add(this.tb_CR_DateRapp);
            this.Controls.Add(this.tb_CR_NumRapp);
            this.Name = "Form4";
            this.Text = "Compte Rendu";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_CR_NumRapp;
        private System.Windows.Forms.TextBox tb_CR_DateRapp;
        private System.Windows.Forms.TextBox tb_CR_Visite;
        private System.Windows.Forms.RichTextBox rtb_CR_Bilan;
        private System.Windows.Forms.Button btn_CR_Details;
        private System.Windows.Forms.Button btn_CR_Precedent;
        private System.Windows.Forms.Button btn_CR_Suivant;
        private System.Windows.Forms.Button btn_CR_Nouveau;
        private System.Windows.Forms.Button btn_CR_Fermer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.ComboBox cb_CR_Praticien;
        private System.Windows.Forms.Button button1;
    }
}